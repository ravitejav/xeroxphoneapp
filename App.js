import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  useColorScheme,
  KeyboardAvoidingView,
} from 'react-native';
import {PersistGate} from 'redux-persist/integration/react';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Provider} from 'react-redux';

import {Login} from './app/components/Login/Login';
import {store, persistor} from './app/store/configureStore';
import {checkForLogin} from './app/Utils/LoginChecker';
import UserStack from './app/UserStack';
import {NavigationContainer} from '@react-navigation/native';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : -900;

  if (checkForLogin()) {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <NavigationContainer>
              <UserStack />
            </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={backgroundStyle}>
          <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
          <KeyboardAvoidingView
            behavior="position"
            keyboardVerticalOffset={keyboardVerticalOffset}>
            <Login />
          </KeyboardAvoidingView>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;
