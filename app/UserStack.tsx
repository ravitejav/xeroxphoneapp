import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Cart} from './components/Cart';

import { paths } from './Utils/Routes';

const Tab = createBottomTabNavigator();

const home = () => (
  <View style={{ backgroundColor: '#a2fba2', display: 'flex', height: '100%', width: '100%' }}>

    <Text>Feed</Text>
  </View>
);
const myCart = () => (<Text>my cart</Text>);
const myOrders = () => (<Text>my orders</Text>);
const Profile = () => (<Text>Profile</Text>);
const startNewCart = () => (<Text>new cart</Text>);


const UserStack = () => {
  return (
    <Tab.Navigator
      initialRouteName={paths.HOME}
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          bottom: 5,
          left: 5,
          right: 5,
          elevation: 0,
          backgroundColor: '#fff',
          borderRadius: 15,
          height: 75,
          ...style.shadow
        }
      }}
    >
      <Tab.Screen
        name={paths.HOME}
        component={home}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={style.view}>
              <MaterialCommunityIcons name="home" size={30} color={focused ? "#e32f45" : "#748c94"} />
              <Text style={{ fontSize: 12, color: focused ? "#e32f45" : "#748c94" }}>Home</Text>
            </View>
          )
        }}
      />
      <Tab.Screen
        name={paths.MY_ORDERS}
        component={myOrders}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={style.view}>
              <MaterialCommunityIcons name="check-box-multiple-outline" size={30} color={focused ? "#e32f45" : "#748c94"} />
              <Text style={{ fontSize: 12, color: focused ? "#e32f45" : "#748c94" }}>My Orders</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={paths.NEW_CART}
        component={Cart}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={style.view}>
              <MaterialCommunityIcons name="cart" size={30} color={focused ? "#e32f45" : "#748c94"} />
              <Text style={{ fontSize: 12, color: focused ? "#e32f45" : "#748c94" }}>New Cart</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={paths.MY_CART}
        component={myCart}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={style.view}>
              <MaterialCommunityIcons name="shopping" size={30} color={focused ? "#e32f45" : "#748c94"} />
              <Text style={{ fontSize: 12, color: focused ? "#e32f45" : "#748c94" }}>My Cart</Text>
            </View>
          )
        }}
      />
      <Tab.Screen
        name={paths.PROFILE}
        component={Profile}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={style.view}>
              <MaterialCommunityIcons name="account-circle" size={30} color={focused ? "#e32f45" : "#748c94"} />
              <Text style={{ fontSize: 12, color: focused ? "#e32f45" : "#748c94" }}>Profile</Text>
            </View>
          )
        }}
      />
    </Tab.Navigator>
  );
}

const style = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5
  },
  view: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  focused: {
    backgroundColor: '#D3D3D3'
  }
});

export default UserStack;