export const paths = {
    HOME: "Home",
    MY_CART: "myCart",
    NEW_CART: "newCart",
    PROFILE: "profile",
    MY_ORDERS: "myOrders"
}