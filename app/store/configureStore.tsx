import { createStore, combineReducers, applyMiddleware } from 'redux';
import LoginReducer from './reducers/loginReducer';
import ReduxThunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const rootReducer = combineReducers(
    { login: LoginReducer }
);

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(ReduxThunk))
const persistor = persistStore(store);

export {store, persistor};