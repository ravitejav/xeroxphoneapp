import * as React from "react";
import { useState } from "react";
import { Text, View, FlatList, TouchableOpacity } from "react-native";
import DocumentPicker from 'react-native-document-picker';
import Toast from 'react-native-simple-toast';
import { Btn } from "../coreComponents/Btn";

import { CartStyles } from "./CartStyles";

export const Cart = () => {

    const [documents, setDocumnets] = useState([]);

    const handleFile = async () => {
        const docs: { id: number; uri: string; type: string; name: string; }[] = [];
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.pdf],
            });
            let i = documents.length;
            for (const res of results) {
                docs.push({
                    id: i,
                    uri: res.uri,
                    type: res.type,
                    name: res.name,
                })
                i++;
            }
            console.log([...documents, docs]);
            setDocumnets((documents) => [...documents, ...docs]);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                Toast.show("Please select, Don't cancel it!!!");
            } else {
                throw err;
            }
        }
    };

    const handleCheckout = () => {

    }

    const removeDoc = (index) => {
        console.log(documents, index);
        setDocumnets((documents) => [...documents.filter(x => x.id != index)]);
    }

    const renderDoc = ({ item, index }) => {

        return (
            <View style={CartStyles.scrollableItem} key={index}>
                <View style={CartStyles.itemDetails}>
                    <Text style={CartStyles.detailsData}>Name: {item.name}</Text>
                </View>
                <TouchableOpacity style={CartStyles.crossButton} onPress={() => removeDoc(item.id)}>
                    <Text style={CartStyles.crossMark}>X</Text>
                </TouchableOpacity>
            </View>
        )
    }


    return (
        <View style={CartStyles.container}>
            <View style={CartStyles.cartHeadingContainer}>
                <Text style={CartStyles.cartHeading}>My Cart</Text>
            </View>
            <View style={documents.length > 0 ? CartStyles.flatListContainer : CartStyles.emptyFlatListContainer}>
                {documents.length > 0 
                    ? <FlatList data={documents} renderItem={renderDoc} keyExtractor={item => item['id']} />
                    : <Text style={CartStyles.noItems}>NO Items</Text>
                }
            </View>
            <View style={CartStyles.buttonContainer}>
                <Btn label={"Choose File"} icon={"cloud-upload"} onClick={handleFile} style={CartStyles.buttonStyle} labelStyles={CartStyles.LabelStyle} />
                <Btn label={"Next Step"} icon={"arrow-right"} onClick={handleCheckout} style={[CartStyles.buttonStyle, CartStyles.nextButtonStyle]} leftSide={false} rightSide={true} labelStyles={CartStyles.LabelStyle} />
            </View>
        </View>
    );
}