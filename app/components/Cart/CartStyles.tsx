import { StyleSheet } from "react-native";

export const CartStyles = StyleSheet.create({
    container: {
        display: "flex",
        backgroundColor: '#d3d3d3', 
        height: '100%', 
        width: '100%',
        alignItems: 'center'
    },
    buttonStyle: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        borderRadius: 15,
        width: 160,
        height: 50,
        backgroundColor: '#3f51b5',
    },
    nextButtonStyle : {
        backgroundColor: '#ff4081'
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        width: '100%',
        flex: 3,
    },
    LabelStyle: {
        color: '#fff'
    },
    cartHeading: {
        fontSize: 24,
        color: '#2578ad',
        fontWeight: "900",
        letterSpacing: 3,
    },
    cartHeadingContainer: {
        flex: 1,
    },
    flatListContainer : {
        flex: 10,
        borderColor: '#fff',
        borderWidth: 2,
        width: '100%',
        padding: 2,
        borderRadius: 15,
    },
    emptyFlatListContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flex: 10,
        borderWidth: 2,
        width: '100%',
        padding: 2,
        borderRadius: 15,
        borderColor: '#fff',
    },
    scrollableItem: {
        width: '100%',
        padding: '1%',
        backgroundColor: '#fff',
        borderRadius: 9,
        height: 60,
        display: "flex",
        flexDirection: "row",
        borderColor: '#87CEFA',
        borderWidth: 2
    },
    itemDetails: {
        flex: 6,
        borderRightColor: '#87CEFA',
        borderRightWidth: 2,
    },
    detailsData: {
        color: '#444',
        padding: 2
    },
    crossButton: {
        flex: 1,
    },
    crossMark: {
        fontSize: 30,
        color: '#87CEFA',
        paddingLeft: 10
    },
    noItems: {
        fontSize: 30,
        color: '#888'
    }
});