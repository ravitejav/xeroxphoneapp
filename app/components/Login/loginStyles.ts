import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        borderWidth: 2,
        height: '100%',
        borderColor: 'black',
        alignItems: "center"
    },
    LoginContainer: {
        flex: 1,
        display: "flex",
        justifyContent: "space-around",
        width: '100%',
        height: '100%',
        alignItems: "center",
        flexDirection: "column"
    },
    inputElement: {
        height: 40,
        borderColor: '#000',
        width: '90%',
        borderWidth: 1,
        borderRadius: 12,
        margin: 12,
    },
    loginHeader: {
        height: 120,
        justifyContent: "center",
        fontSize: 40,
        margin: 'auto'
    },
    ButtonElement: {
        height: 40,
        borderColor: '#000',
        borderRadius: 12,
        backgroundColor: '#08f',
        width: 150,
        margin: 'auto'
    },
    orText: {
        height: 20,
        
    }
});

export { styles };