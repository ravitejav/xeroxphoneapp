import { Text, TouchableOpacity, View } from "react-native";
import * as React from "react";
import { Input } from "../coreComponents/Input";
import { Btn } from "../coreComponents/Btn";
import { styles } from './loginStyles';
import { title } from "../../Types/constant";
import { useState } from "react";
import { validateEmail } from "../../Utils/validators";
import Toast from 'react-native-simple-toast';

export const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [signUpEmail, setSignUpEmail] = useState('');
    const [signUpPassword, setSignUpPassword] = useState('');
    const [name, setName] = useState('');
    const [showOtp, setShowOtp] = useState(false);
    const [OTP, setOTP] = useState('');


    const [loginPage, setLoginPage] = useState(true);

    const loginUser = () => {
        if(email && !validateEmail(email)) {
            Toast.show("Invalid Email!!", Toast.SHORT);
            return ;
        }
        if(password.length < 8) {
            Toast.show("Password must be 8 characters!!", Toast.SHORT);
            return ;
        }
        // login api call
    }

    const signUpUser = () => {
        if(signUpEmail && !validateEmail(signUpEmail)) {
            Toast.show("Invalid Email!!", Toast.SHORT);
            return ;
        }
        if(signUpPassword.length < 8) {
            Toast.show("Password must be 8 characters!!", Toast.SHORT);
            return ;
        }
        if(name.length < 4) {
            Toast.show("Name is too small!!", Toast.SHORT);
            return ;
        } 
        if(!showOtp) {
            Toast.show("OTP sent to your mail", Toast.SHORT);
            setShowOtp(true);
        }
        //sign up api call
    }

    const clearSignUP = () => {
        setLoginPage(true);
        setShowOtp(false);
        setSignUpEmail('');
        setSignUpPassword('');
        setName('');
        setOTP('');
    }

    const clearSignIn = () => {
        setLoginPage(false);
        setEmail('');
        setPassword('');
    }

    if(loginPage) {
        return (
            <View style={styles.container}>
                <View style={styles.LoginContainer}>
                    <Text style={styles.loginHeader}>Login</Text>
                    <View style={{ width: '100%', display: "flex", alignItems: "center" }}>
                        <Input style={styles.inputElement} placeHolder={title.EMAIL} onChange={(email: React.SetStateAction<string>) => { setEmail(email); }} />
                        <Input style={styles.inputElement} placeHolder={title.PASSWORD} onChange={(password: React.SetStateAction<string>) => { setPassword(password) }} />
                        <Btn style={styles.ButtonElement} onClick={loginUser} label={title.LOGIN} />
                    </View>
                    <TouchableOpacity onPress={() => clearSignIn()}>
                        <Text>Not a member? SignUp</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    return (
        <View style={styles.container}>
            <View style={styles.LoginContainer}>
                <Text style={styles.loginHeader}>Sign Up</Text>
                <View style={{ width: '100%', display: "flex", alignItems: "center" }}>
                    <Input style={styles.inputElement} placeHolder={title.NAME} onChange={(name: React.SetStateAction<string>) => { setName(name); }} />
                    <Input style={styles.inputElement} placeHolder={title.EMAIL} onChange={(email: React.SetStateAction<string>) => { setSignUpEmail(email) }} />
                    <Input style={styles.inputElement} placeHolder={title.PASSWORD} onChange={(password: React.SetStateAction<string>) => { setSignUpPassword(password) }} />
                    {showOtp && <Input style={styles.inputElement} placeHolder={title.OTP} onChange={(otp: React.SetStateAction<string>) => { setOTP(otp) }} />}
                    <Btn style={styles.ButtonElement} onClick={signUpUser} label={title.SIGNUP} />
                </View>
                <TouchableOpacity style={{margin: 12}} onPress={() => clearSignUP()}>
                    <Text>Already Memeber? SignIn</Text>
                </TouchableOpacity>
            </View>
        </View>
    );;
}