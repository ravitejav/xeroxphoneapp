import * as React from "react";
import { buttonProps } from "../../Types/propTypes";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const Btn = (props: buttonProps) => {
    const {
        onClick,
        style,
        label,
        labelStyles,
        icon,
        iconStyles = { size: 30, color: '#fff' },
        leftSide = true,
        rightSide = false,
    } = props;

    return (
        <TouchableOpacity style={[BtnStyles.btnContainer, style]} onPress={onClick}>
            {icon && leftSide && !rightSide && <MaterialCommunityIcons name={icon} size={iconStyles.size} color={iconStyles.color} />}
            <Text style={[BtnStyles.text, labelStyles]}>{label}</Text>
            {icon && !leftSide && rightSide && <MaterialCommunityIcons name={icon} size={iconStyles.size} color={iconStyles.color} />}
        </TouchableOpacity>
    )
}

const BtnStyles = StyleSheet.create({
    text: {
        fontSize: 20,
    },
    btnContainer: {
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center'
    }
})