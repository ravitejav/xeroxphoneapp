import * as React from "react";
import { inputProps } from "../../Types/propTypes";
import { TextInput } from "react-native";
import { title } from "../../Types/constant";

export const Input = (props: inputProps) => {
    const {
        placeHolder,
        onChange,
        style,
    } = props;

    return (
        <TextInput onChangeText={onChange} style={style} placeholder={placeHolder} secureTextEntry={placeHolder == title.PASSWORD} />
    )
}