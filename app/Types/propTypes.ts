interface inputProps {
    onChange: any,
    label: string,
    placeHolder: string,
    value: any,
    style: any,
}

interface buttonProps {
    onClick: any,
    label: string,
    style: any,
    labelStyles: any,
    icon: string,
    iconStyles: {
        color: string,
        size: number,
    },
    leftSide: boolean,
    rightSide: boolean
}

export {inputProps, buttonProps};