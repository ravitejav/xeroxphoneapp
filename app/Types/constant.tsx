export enum title {
    NAME = 'Full Name',
    EMAIL = "Email ID",
    PASSWORD = "Password",
    LOGIN = "Login",
    SIGNUP = "Sign Up",
    OTP = "OTP"
}